
import { setCycles, setTime } from '../store/actions/actions';
import { SET_TIME_ASYNC, SET_CYCLES_ASYNC } from '../store/actions/actionTypes';
import { takeEvery, put } from 'redux-saga/effects';

function* setCyclesAsync(action) {
  yield put(setCycles(action.data));
}

function* setTimeAsync(action) {
  yield put(setTime(action.key, action.data));
}

export default function* rootSaga() {
  yield takeEvery(SET_CYCLES_ASYNC, setCyclesAsync);
  yield takeEvery(SET_TIME_ASYNC, setTimeAsync);
}