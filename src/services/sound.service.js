import Sound from 'react-native-sound';

import { store } from '../../index';

export const SOUNDS = {
  FINAL: 'FINAL',
  TA_DA: 'TA_DA',
  BEEP: 'BEEP',
};

class SoundService {
  constructor() {
    const final = new Sound('beep_last.mp3', Sound.MAIN_BUNDLE);
    const taDa = new Sound('beep_tada.mp3', Sound.MAIN_BUNDLE);
    const beep = new Sound('beep.mp3', Sound.MAIN_BUNDLE);

    this.sounds = {
      [SOUNDS.FINAL]: final,
      [SOUNDS.TA_DA]: taDa,
      [SOUNDS.BEEP]: beep,
    };
  }

  playSound = (sound) => {
    const { soundMuted } = store.getState();
    if (!soundMuted) {
      this.sounds[sound].play();
    }
  }
}

export default SoundService;
