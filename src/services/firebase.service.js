import { admob } from 'react-native-firebase';

class FirebaseService {
  constructor() {
    const { AdRequest } = admob;
    const adRequestInstance = new AdRequest();
    this.request = adRequestInstance.build();
  }

  getAdRequest = () => this.request
}

export default FirebaseService;
