import { Dimensions, PixelRatio } from 'react-native';

export const secondsToTime = (seconds, string) => {
  const h = Math.floor(seconds / 3600);
  const m = Math.floor((seconds % 3600) / 60);
  const s = seconds % 60;

  const time = [h, m > 9 ? m : h ? '0' + m : '0' + m || '0', s > 9 ? s : '0' + s];
  if (string) {
    return time.filter(a => a).join(':');
  }
  return time.filter((a, index) => ({ [index]: a }));
};

export const getTimeInSeconds = (stateField, minutes, seconds) => {
  const values = [
    minutes[stateField[0]],
    seconds[stateField[1]],
  ];

  const timeMultipliers = [60,1];
  return values
    .map((place, index) => parseInt(place, 10) * timeMultipliers[index])
    .reduce((a, b) => a + b);
}

export const generateNumbers = (from, to, unit) => {
  const numbers = [];

  for (let i = from; i < to + 1; i++) {
    const number = i.toString();

    numbers.push(number.length === 1 ? `0${number}` : number);
  }
  if (unit) {
    return numbers.map(number => `${number} ${unit}`).reverse();
  }
  return numbers.reverse();
}

export const isTablet = () => {
  let pixelDensity = PixelRatio.get();
  const adjustedWidth = Dimensions.get('window').width * pixelDensity;
  const adjustedHeight = Dimensions.get('window').height * pixelDensity;

  if (pixelDensity < 2 && (adjustedWidth >= 1000 || adjustedHeight >= 1000)) {
    return true;
  } else if (pixelDensity < 2) {
    return true;
  } else {
    return (
      pixelDensity === 2 && (adjustedWidth >= 1920 || adjustedHeight >= 1920)
    );
  }
};
