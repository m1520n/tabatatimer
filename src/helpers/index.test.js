import { secondsToTime } from './index';

it('returns formatted time string when passed seconds integer', () => {
  [
    { seconds: 120, string: true, expected: '02:00' },
    { seconds: 362, string: true, expected: '06:02' },
    { seconds: 3729, string: true, expected: '1:02:09' },
    { seconds: 728, string: false, expected: [0, 12, '08'] },
  ].forEach(({ seconds, string, expected }) => {
    expect(secondsToTime(seconds, string)).toEqual(expected);
  })
});
