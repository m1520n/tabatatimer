import { PixelRatio } from 'react-native';
import { isTablet } from '../helpers';
import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const tablet = isTablet;

export const colors = {
  white: '#ECF0F1',
  yellow: '#FDFF00',
  gray: '#34495E',
  grayShadow: '#2B3C4E',
  darkerGray: '#535B5E',
  red: '#F60957',
  redShadow: '#DD084E',
  blue: '#3498DB',
  blueShadow: '#217DBB',
  green: '#1DBC60',
  greenShadow: '#1AA655',
  black: '#1C262F',
};

export const sizes = {
  wheelpicker: PixelRatio.getPixelSizeForLayoutSize(wp('8%')),
  text0: wp('20%'),
  text1: wp('15%'),
  text2: wp('12%'),
  text3: wp('10%'),
  text4: wp('7%'),
  text5: wp('5%'),
  heading1: wp('10%'),
}

export const spacings = {
  radius: wp('2%'),
  margin2: wp('8%'),
  margin3: wp('1.5%'),
  padding1: wp('15%'),
  padding2: wp('5%'),
  padding3: wp('3%'),
  padding4: wp('1%'),
  wpHeight: wp('50%'),
  circle: wp('85%'),
  wpWidth: wp('21%'),
  paddingCircular: wp('4%'),
  panelSize: tablet() ? 235 : 280,
}
export const svg = {
  SettingsIcon: wp('15%'),
  mainButton: tablet() ? 83.33 : 100,
  Logo: 250,
  panelWidth: tablet() ? 250 : 300,
  panelHeight: tablet() ? 100 : 120,
  sideButton: tablet() ? 50 : 60,
}

export const viewBox = {
  view1: `0 0 ${svg.SettingsIcon * 16} ${svg.SettingsIcon * 16}`,
  viewMainButton: tablet() ? `0 0 ${svg.mainButton * 6} ${svg.mainButton * 6}`: `0 0 ${svg.mainButton * 5} ${svg.mainButton * 5}`,
  viewLogo: `0 0 ${svg.Logo * 2} ${svg.Logo * 2}`,
  viewPanel: tablet() ? `0 0 ${svg.panelWidth * 6} ${svg.panelHeight * 6}` : `0 0 ${svg.panelWidth * 5} ${svg.panelHeight * 5}`,
  viewSideButton: tablet() ? `0 0 ${svg.sideButton * 10} ${svg.sideButton * 10}` : `0 0 ${svg.sideButton * 8.33} ${svg.sideButton * 8.33}`,
}

export const fonts = {
  rubik: 'Rubik-Regular',
  rubikBold: 'Rubik-Bold',
  rubikBoldItalic: 'Rubik-BoldItalic',
  rubikMedium: 'Rubik-Medium',
  rubikMediumItalic: 'Rubik-MediumItalic',
};
