import {
  SET_CYCLES,
  SET_TIME,
  TOGGLE_SOUND_MUTE,
} from '../actions/actionTypes';

import { generateNumbers } from '../../helpers';

const initialState = {
  prepareTime: [59, 54],
  exerciseTime: [59, 29],
  restTime: [59, 54],
  cycles: 98,
  minutes: generateNumbers(0, 59, 'm'),
  seconds: generateNumbers(0, 59, 's'),
  unitData: generateNumbers(1, 99),
  soundMuted: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case SET_TIME:
    return {
      ...state,
      [action.key]: [...action.data],
    }

  case SET_CYCLES:
    return {
      ...state,
      cycles: action.data,
    }

  case TOGGLE_SOUND_MUTE:
    return {
      ...state,
      soundMuted: action.currentValue,
    }

  default:
    return {
      ...state,
    };
  }
};

export default reducer;
