import {
  SET_TIME,
  SET_CYCLES,
  SET_CYCLES_ASYNC,
  SET_TIME_ASYNC,
  TOGGLE_SOUND_MUTE,
} from './actionTypes';

// time actions
export const setTime = (key, data) => ({
  type: SET_TIME,
  key,
  data,
});

export const setTimeAsync = (key, data) => ({
  type: SET_TIME_ASYNC,
  key,
  data,
});

// cycle actions
export const setCycles = data => ({
  type: SET_CYCLES,
  data,
});

export const setCyclesAsync = data => ({
  type: SET_CYCLES_ASYNC,
  data,
});

export const setSoundMuted = value => ({
  type: TOGGLE_SOUND_MUTE,
  currentValue: value,
})

