export const SET_CYCLES = 'SET_CYCLES';
export const SET_TIME = 'SET_TIME';
export const SET_CYCLES_ASYNC = 'SET_CYCLES_ASYNC';
export const SET_TIME_ASYNC= 'SET_TIME_ASYNC';
export const TOGGLE_SOUND_MUTE = 'TOGGLE_SOUND_MUTE';