import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, TouchableNativeFeedback, Alert, Switch } from 'react-native';

import { setSoundMuted } from '../store/actions/actions';
import styled from 'styled-components';

import { colors, spacings, sizes, fonts } from '../styles/styles';

import { settings } from '../texts/texts.json';

const propTypes = {
  soundMuted: PropTypes.bool.isRequired,
  setSoundMuted: PropTypes.func.isRequired,
  navigation: {
    navigate: PropTypes.func.isRequired,
  }.isRequired,
};

const Container = styled(View)`
  width: 100%;
  height: 100%;
  padding-top: ${spacings.padding2};
  padding-left: ${spacings.padding2};
  background-color: ${colors.black};
  align-items: center;
  justify-content: space-between;
`;

const StyledText = styled(Text)`
  font-size: ${sizes.heading1};
  color: ${colors.white};
  font-family: ${fonts.rubikBold};
`;

const Close = styled(Text)`
  font-size: ${sizes.heading1};
  color: ${colors.red};
  font-family: ${fonts.rubikBold};
  text-align: right;
  align-self: stretch;
  margin-right: 10%;
`;

const ScreenName = styled(View)`
  height: 10%;
  width: 100%;
  align-items: center;
`;


const OptionsContainer = styled(View)`
  height: 90%;
  width: 100%;
`;

const SectionName = styled(Text)`
  height: 8%;
  justify-content: center;
  color: ${colors.red};
  font-size: ${sizes.text4};
  font-family: ${fonts.rubikMediumItalic};
  padding-left: ${spacings.padding3};
  border-bottom-color: ${colors.gray};
  border-bottom-width: .2;
`;

const Section = styled(View)`
  height: 12%;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

const Name = styled(Text)`
  color: ${colors.white};
  font-size: ${sizes.text5};
  font-family: ${fonts.rubikMedium};
  padding-left: ${spacings.padding2};

`;

const Controller = styled(View)`
  padding-right: ${spacings.padding2};
`;

class Settings extends React.Component {
  handleSoundMuted = value => {
    this.props.setSoundMuted(value);
  }

  handleClose = () => {
    this.props.navigation.navigate('BasicTabata', {});
  }

  render() {
    return (
      <Container>
        <TouchableNativeFeedback onPress={this.handleClose}>
          <Close>x</Close>
        </TouchableNativeFeedback>
        <ScreenName>
          <StyledText>Settings</StyledText>
        </ScreenName>

        <OptionsContainer>
          <SectionName>
            Sounds
          </SectionName>
          <Section>
            <Name>mute sounds</Name>
            <Controller>
              <Switch
                value={this.props.soundMuted}
                onValueChange={this.handleSoundMuted}
                thumbColor={colors.red}
              />
            </Controller>
          </Section>

          <SectionName>
            About
          </SectionName>
          <TouchableNativeFeedback onPress={() => {
            Alert.alert(
              'Privacy & policy',
              settings.about,
              [
                { text: 'OK' },
              ],
              { cancelable: true },
            );
          }}>
            <Section>
              <Name>Privacy & policy</Name>
            </Section>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback onPress={() => {
            Alert.alert(
              'Terms & Conditions',
              settings.termsAndConditions,
              [
                {text: 'OK'},
              ],
              { cancelable: true },
            );
          }}>
            <Section>
              <Name>Terms & Conditions</Name>
            </Section>
          </TouchableNativeFeedback>
        </OptionsContainer>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  soundMuted: state.soundMuted,
})

const mapDispatchToProps = {
  setSoundMuted,
}

Settings.propTypes = propTypes;

const ConnectedSettings = connect(mapStateToProps, mapDispatchToProps)(Settings);
export default ConnectedSettings;
