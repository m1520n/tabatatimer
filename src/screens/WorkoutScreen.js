import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import styled from 'styled-components';
import { CircularProgress } from 'react-native-circular-progress';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';

import { secondsToTime } from '../helpers';
import { colors, fonts, sizes, spacings, svg, viewBox } from '../styles/styles';

import ControlPanel from '../svg/ControlPanel';
import Start from '../svg/Start';
import Stop from '../svg/Stop';
import Refresh from '../svg/Refresh';
import Exit from '../svg/Exit';

const propTypes = {
  seconds: PropTypes.number.isRequired,
  totalCycles: PropTypes.number.isRequired,
  cycle: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  stepColor: PropTypes.string.isRequired,
  nextStep: PropTypes.func.isRequired,
  playSound: PropTypes.func.isRequired,
  navigation: {
    navigate: PropTypes.func.isRequired,
  }.isRequired,
};

const MainContainer = styled(View)`
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  height: 100%;
  background-color: ${({ tintColor }) => tintColor};
`;

const Circular = styled(CircularProgress)`
  height: 60%;
  align-self: center;
`;

const PanelContainer = styled(View)`
  height: 25%;
  width: ${spacings.panelSize};
  position: relative;
  flex-direction:row;
  align-items: center;
  justify-content: space-between;
`;

const PanelButton = styled(View)`
  z-index: 100;
`;

const StyledControlPanel = styled(View)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const ProgressContainer = styled(View)`
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: ${colors.black};
`;

const Step = styled(Text)`
  width: 100%;
  font-family: ${fonts.rubikMediumItalic};
  font-size: ${sizes.text2};
  color: ${colors.white};
  line-height: ${sizes.text2};
  text-align: center;
`;

const Time = styled(Text)`
  font-family: ${fonts.rubikMedium};
  font-size: ${sizes.text0};
  color: ${colors.white};
  line-height: ${sizes.text0};
  width: 40%;
  text-align: ${({ align }) => align };
`;

const Seperator = styled(Text)`
  font-family: ${fonts.rubikMedium};
  font-size: ${sizes.text0};
  color: ${colors.white};
  line-height: ${sizes.text0};
`;

const CounterContainer = styled(View)`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Cycles = styled(Text)`
  font-family: ${fonts.rubikBold};
  font-size: ${sizes.text5};
  color: ${({ tintColor }) => tintColor};
  text-align: center;
`;

class WorkoutScreen extends Component {
  constructor(props) {
    super(props);

    if (props.seconds === 0) {
      props.nextStep();
    }
    this.state = {
      counter: props.seconds,
      paused: false,
    };

  }

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  startTimer = () => {
    this.interval = setInterval(() => {
      if (this.state.counter === 0) {
        clearInterval(this.interval);
        this.props.nextStep();
      } else {
        this.setState({ counter: this.state.counter - 1 }, () => {
          if (this.state.counter < 4 && this.state.counter > 0) {
            this.props.playSound();
          }
          if (this.state.counter === 0) {
            this.props.nextStep();
          }
        });
      }
    }, 1000);
  };

  handlePause = () => {
    if (this.state.paused) {
      this.setState({
        paused: false,
      }, () => { this.startTimer() });
    } else {
      this.setState({
        paused: true,
      }, () => { clearInterval(this.interval) });
    }
  }

  handleGoBack = () => {
    this.props.navigation.navigate('BasicTabata');
    return true;
  }

  handleRefreshTime = () => {
    this.setState({
      counter: this.props.seconds,
    })
  }
  render() {
    const {
      seconds,
      cycle,
      totalCycles,
      name,
      stepColor,
    } = this.props;
    const { counter, paused } = this.state;
    const initialCount = seconds;

    // This is needed to prevent division by zero error
    const percent = initialCount === 0 ? 100 : ((initialCount - counter) / initialCount) * 100;
    const timer = secondsToTime(counter);
    return (
      <MainContainer tintColor={stepColor}>
        <Circular
          size={spacings.circle}
          width={spacings.padding2}
          backgroundWidth={spacings.paddingCircular}
          fill={percent}
          tintColor={colors.yellow}
          backgroundColor={colors.gray}
          rotation={0}
        >
          {() => (
            <ProgressContainer>
              <CounterContainer>
                <Time align='right'>{timer[1]}</Time>
                <Seperator>:</Seperator>
                <Time align='left'>{timer[2]}</Time>
              </CounterContainer>
              <Step>{name}</Step>
              <Cycles tintColor={stepColor}>
                Round {cycle} of {totalCycles}
              </Cycles>
            </ProgressContainer>
          )}
        </Circular>
        <PanelContainer>
          <StyledControlPanel>
            <ControlPanel height={svg.panelHeight} width={svg.panelWidth} fill={colors.gray} viewBox={viewBox.viewPanel}/>
          </StyledControlPanel>
          <PanelButton>
            <TouchableWithoutFeedback onPress={this.handleGoBack}>
                <Exit height={svg.sideButton} width={svg.sideButton} fill={colors.yellow} viewBox={viewBox.viewSideButton}/>
            </TouchableWithoutFeedback>
          </PanelButton>

          <PanelButton>
            <TouchableWithoutFeedback onPress={this.handlePause}>
              { paused ?
                <Start height={svg.mainButton} width={svg.mainButton} fill={colors.yellow} viewBox={viewBox.viewMainButton}/>
                :  <Stop height={svg.mainButton} width={svg.mainButton} fill={colors.yellow} viewBox={viewBox.viewMainButton}/> }
            </TouchableWithoutFeedback>
          </PanelButton>

          <PanelButton>
            <TouchableWithoutFeedback onPress={this.handleRefreshTime}>
                <Refresh height={svg.sideButton} width={svg.sideButton} fill={colors.yellow} viewBox={viewBox.viewSideButton}/>
            </TouchableWithoutFeedback>
          </PanelButton>
        </PanelContainer>
      </MainContainer>
    );
  }
}

WorkoutScreen.propTypes = propTypes;

export default withNavigation(WorkoutScreen);
