import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import firebase from 'react-native-firebase';
import KeepAwake from 'react-native-keep-awake';

import { SOUNDS } from '../services/sound.service';

import WorkoutScreen from './WorkoutScreen';

import { colors } from '../styles/styles';

const propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func,
    navigate: PropTypes.func,
  }).isRequired,
};

const Banner = firebase.admob.Banner;

const Advertisement = styled(Banner)`
  position: absolute;
  top: 0;
  width: 100%;
  height: 15%;
  z-index: 100;
`;
class StateMachine extends Component {
  constructor(props) {
    super(props);

    this.seconds = props.navigation.getParam('seconds');
    this.totalCycles = props.navigation.getParam('cycles');
    this.adRequest = props.navigation.getParam('adRequest');
    this.playSound = props.navigation.getParam('playSound');

    this.steps = [
      {
        name: 'Prepare!',
        seconds: this.seconds[0],
        stepColor: colors.blue,
      },
      {
        name: 'Workout!',
        seconds: this.seconds[1],
        stepColor: colors.red,
      },
      {
        name: 'Rest!',
        seconds: this.seconds[2],
        stepColor: colors.green,
      },
    ];

    this.state = {
      step: 0,
      cycle: 1,
      done: false,
    };
  }

  nextStep = () => {
    const { step, cycle } = this.state;
    const stepsLength = this.steps.length;

    const isNotLastStep = step + 1 < stepsLength;
    const isNotLastCycle = cycle + 1 <= this.totalCycles;

    if (!isNotLastCycle && step + 1 === 2) {
      this.setState({
        done: true,
      }, () => {
        this.playSound(SOUNDS.TA_DA);
      })
      this.props.navigation.navigate('ResultsScreen');
    } else if (isNotLastStep) {
      this.setState({
        step: step + 1,
      }, () => {
        this.playSound(SOUNDS.FINAL);
      });
    // It is a last step but there are still cycles
    } else if (isNotLastCycle) {
      this.setState({
        step: 1,
        cycle: cycle + 1,
      }, () => {
        this.playSound(SOUNDS.FINAL);
      });
    }
  }

  handlePlaySound = () => {
    this.playSound(SOUNDS.BEEP);
  }

  render() {
    const { step, cycle, done } = this.state;
    const currentStep = this.steps[step];
    const { name, seconds, stepColor } = currentStep;

    return (
      <>
        <Advertisement
          unitId='ca-app-pub-4797389875104873/1107930634'
          size={'SMART_BANNER'}
          request={this.adRequest}
        />
        <WorkoutScreen key={`${cycle}-${step}`}
          nextStep={this.nextStep}
          done={done}
          seconds={seconds}
          cycle={cycle}
          totalCycles={this.totalCycles}
          name={name}
          stepColor={stepColor}
          playSound={this.handlePlaySound}
        />
        <KeepAwake/>
      </>
    );
  }
}

StateMachine.propTypes = propTypes;

export default StateMachine;
