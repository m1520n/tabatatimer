import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { BasicTabata, SettingsButton } from './BasicTabata';
import ExcerciseStep from '../components/ExerciseStep';
import Cycles from '../components/Cycles';

import renderer from 'react-test-renderer';

import { generateNumbers } from '../helpers';

const navigationMock = {
  navigate: jest.fn(),
};

const renderComponent = ({
  navigation = navigationMock,
  setTimeAsync = jest.fn(),
  setCyclesAsync = jest.fn(),
  restTime = [59, 55],
  exerciseTime = [59, 55],
  prepareTime = [59, 55],
  cycles = 5,
}) => renderer.create(
  <BasicTabata
    setTimeAsync={setTimeAsync}
    cycles={cycles}
    restTime={restTime}
    exerciseTime={exerciseTime}
    prepareTime={prepareTime}
    setCyclesAsync={setCyclesAsync}
    navigation={navigation}
    minutes={generateNumbers(0, 59, 'm')}
    seconds={generateNumbers(0, 59, 's')}
    unitData={generateNumbers(1, 99)}
  />
);

it('renders without crashing', () => {
  expect(renderComponent({}).toJSON()).toBeTruthy();
});

it('calls navigate function with proper params when settings icon is clicked', () => {
  const navigation = {
    navigate: jest.fn(),
  };
  renderComponent({ navigation }).root.findByType(SettingsButton).props.onPress();
  expect(navigation.navigate).toHaveBeenCalledWith('Settings', {});
});

it('calls setTimeAsync with proper params when onUpdateTime callback is called from ExcerciseStep component', () => {
  const setTimeAsync = jest.fn();
  renderComponent({ setTimeAsync }).root.findAllByType(ExcerciseStep)[0].props.onUpdateTime({ position: 1 }, 1, 'prepareTime');
  expect(setTimeAsync).toHaveBeenCalledWith('prepareTime', [59, 1]);
});

it('calls setTimeAsync with proper params when onUpdateTime callback is called from ExcerciseStep component', () => {
  const setTimeAsync = jest.fn();
  renderComponent({ setTimeAsync }).root.findAllByType(ExcerciseStep)[1].props.onUpdateTime({ position: 1 }, 0, 'exerciseTime');
  expect(setTimeAsync).toHaveBeenCalledWith('exerciseTime', [1, 55]);
});

it('calls setCyclesAsync with proper params when onUpdateCycles callback is called from Cycles component', () => {
  const setCyclesAsync = jest.fn();
  renderComponent({ setCyclesAsync }).root.findByType(Cycles).props.onUpdateCycles({ position: 10 });
  expect(setCyclesAsync).toHaveBeenCalledWith(10);
});

it('calls navigate function with proper params when GO button is clicked', () => {
  const navigation = {
    navigate: jest.fn(),
  };
  renderComponent({
    navigation,
    prepareTime: [20, 34],
    exerciseTime: [10, 33],
    restTime: [9, 0],
    cycles: 80,
  }).root.findByType(TouchableWithoutFeedback).props.onPress();
  expect(navigation.navigate).toHaveBeenCalledWith('StateMachine', {
    cycles: 19,
    seconds: [2365, 2966, 3059],
  });
});
