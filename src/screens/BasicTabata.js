import React from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import {
  setCyclesAsync,
  setTimeAsync,
} from '../store/actions/actions';

import {
  secondsToTime,
  getTimeInSeconds,
} from '../helpers';

import TotalTime from '../components/TotalTime';
import Cycles from '../components/Cycles';
import ExerciseStep from '../components/ExerciseStep';

import SettingsIcon from '../svg/SettingsIcon';

import { colors, fonts, sizes, spacings, svg, viewBox } from '../styles/styles';

const propTypes = {
  setTimeAsync: PropTypes.func.isRequired,
  cycles: PropTypes.number.isRequired,
  restTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  exerciseTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  prepareTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  setCyclesAsync: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    addListener: PropTypes.func,
  }).isRequired,
  minutes: PropTypes.arrayOf(PropTypes.string).isRequired,
  seconds: PropTypes.arrayOf(PropTypes.string).isRequired,
  unitData: PropTypes.arrayOf(PropTypes.string).isRequired,
};

const MainContainer = styled(View)`
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  padding-bottom: 3%;
  background-color: ${colors.black};
`;

const StepsContainer = styled(View)`
  align-items: center;
  justify-content: flex-end;
  width: 90%;
  padding-bottom: 3%;
`;

export const SettingsButton = styled(TouchableOpacity)`
  color: ${colors.white};
  position: absolute;
  right: 0;
  top: 5%;
`;

const Go = styled(View)`
  width: 90%;
  height: 16%;
  background-color: ${colors.yellow};
  justify-content: center;
  align-items: center;
  border-radius: ${spacings.radius};
`;

const StyledText = styled(Text)`
  color: ${colors.black};
  font-size: ${sizes.text1};
  font-family: ${fonts.rubikBoldItalic};
  width: 100%;
  text-align: center;
`;

class BasicTabata extends React.Component {
  handleUpdateTime = (e, index, stateField) => {
    // This value is a picker position and has to be mapped to a value later on
    const time = this.props[stateField];
    const newTime = [...time];

    newTime[index] = parseInt(e.position, 10);
    this.props.setTimeAsync(stateField, newTime);
  };

  handleNavigateSettings = () => {
    this.props.navigation.navigate('Settings', {});
  }

  handleUpdateCycles = ({ position }) => {
    this.props.setCyclesAsync(parseInt(position, 10));
  };

  handleNavigate = () => {
    this.props.navigation.navigate('StateMachine', {
      seconds: [
        getTimeInSeconds(this.props.prepareTime, this.props.minutes, this.props.seconds),
        getTimeInSeconds(this.props.exerciseTime, this.props.minutes, this.props.seconds),
        getTimeInSeconds(this.props.restTime, this.props.minutes, this.props.seconds),
      ],
      cycles: parseInt(this.props.unitData[this.props.cycles], 10),
      request: this.request,
    });
  };

  render() {
    const {
      cycles,
      prepareTime,
      restTime,
      exerciseTime,
      minutes,
      seconds,
      unitData,
    } = this.props
    const totalTime =
      getTimeInSeconds(prepareTime, minutes, seconds) + (
        getTimeInSeconds(exerciseTime, minutes, seconds) +
        getTimeInSeconds(restTime, minutes, seconds)
      ) * unitData[cycles];

    const timeString = secondsToTime(totalTime, 'string');

    return (
      <MainContainer>
        <SettingsButton onPress={this.handleNavigateSettings}>
          <SettingsIcon height={svg.SettingsIcon} width={svg.SettingsIcon} fill={colors.yellow} viewBox={viewBox.view1} />
        </SettingsButton>
        <StepsContainer>
          <TotalTime totalTime={timeString} />

          <ExerciseStep
            minutes={minutes}
            seconds={seconds}
            time={prepareTime}
            onUpdateTime={this.handleUpdateTime}
            stateField='prepareTime'
            bgColor={colors.blue}
            shadowColor={colors.blueShadow}
            heading='Prepare'
          />

          <ExerciseStep
            minutes={minutes}
            seconds={seconds}
            time={exerciseTime}
            onUpdateTime={this.handleUpdateTime}
            stateField='exerciseTime'
            bgColor={colors.red}
            shadowColor={colors.redShadow}
            heading='Workout'
          />

          <ExerciseStep
            minutes={minutes}
            seconds={seconds}
            time={restTime}
            onUpdateTime={this.handleUpdateTime}
            stateField='restTime'
            bgColor={colors.green}
            shadowColor={colors.greenShadow}
            heading='Rest'
          />

          <Cycles
            heading='Rounds'
            bgColor={colors.gray}
            shadowColor={colors.grayShadow}
            unitData={unitData}
            cycles={cycles}
            onUpdateCycles={this.handleUpdateCycles}
          />
        </StepsContainer>
        <TouchableWithoutFeedback onPress={this.handleNavigate}>
          <Go>
            <StyledText>GO!</StyledText>
          </Go>
        </TouchableWithoutFeedback>
      </MainContainer>
    );
  }
}

BasicTabata.propTypes = propTypes;

const mapStateToProps = state => ({
  cycles: state.cycles,
  restTime: state.restTime,
  exerciseTime: state.exerciseTime,
  prepareTime: state.prepareTime,
  minutes: state.minutes,
  seconds: state.seconds,
  unitData: state.unitData,
})

const mapDispatchToProps = {
  setCyclesAsync,
  setTimeAsync,
}

const ConnectedBasicTabata = connect(mapStateToProps,mapDispatchToProps)(BasicTabata);

export {
  BasicTabata
};

export default ConnectedBasicTabata;
