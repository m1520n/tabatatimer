import React, { Component } from 'react';
import { View, Animated } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Logo from '../svg/Logo';
import { colors, fonts, sizes, viewBox, svg } from '../styles/styles';

const propTypes = {
  navigation: PropTypes.shape({
    dispatch: PropTypes.func.isRequired,
  }).isRequired,
};

const MainContainer = styled(View)`
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  background-color: ${colors.black};
`;

const StyledText = styled(Animated.Text)`
  font-family: ${fonts.rubikBoldItalic};
  font-size: ${sizes.text1};
  color: ${colors.white};
`;

const StyledView =  styled(Animated.View)`
  align-items: center;
  justify-content: center;
`;

class SplashScreen extends Component {
  constructor() {
    super();

    this.state = {
      slideAnimation: new Animated.Value(-700),
      rotateAnimation: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.spring(this.state.slideAnimation, {
      toValue: 0,
      duration: 3000,
      delay: 0,
      useNativeDriver: true,
    }).start();
    Animated.loop(
      Animated.timing(this.state.rotateAnimation, {
        toValue: 1,
        duration: 4000,
        useNativeDriver: true,
      })
    ).start();

    setTimeout(() => {
      this.handleNavigate();
    }, 2000);
  }

  handleNavigate = () => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions:
        [
          NavigationActions.navigate({
            routeName: 'BasicTabata',
          }),
        ],
    });

    this.props.navigation.dispatch(navigateAction);
  };

  render() {
    const {
      rotateAnimation,
      slideAnimation,
    } = this.state;

    const rotation = rotateAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });

    return (
      <MainContainer>
        <StyledView>
          <StyledText style={{
            transform: [
              {
                translateX: slideAnimation,
              },
            ],
          }} >
            Trainingo
          </StyledText>

          <StyledView style={{
            transform: [
              {
                translateX: slideAnimation,
              },
              {
                rotate: rotation,
              },
            ],
          }}>
            <Logo
              viewBox={viewBox.viewLogo}
              width={svg.Logo}
              height={svg.Logo}
            />
          </StyledView>
        </StyledView>
      </MainContainer>
    );
  }
}

SplashScreen.propTypes = propTypes;

export default SplashScreen;
