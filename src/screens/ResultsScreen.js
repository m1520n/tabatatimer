import React, { Component } from 'react';
import { BackHandler, TouchableWithoutFeedback, View, Text } from 'react-native';
import { connect } from 'react-redux';

import styled from 'styled-components';
import PropTypes from 'prop-types';

import { getTimeInSeconds, secondsToTime } from '../helpers';
import { colors, fonts, sizes, spacings } from '../styles/styles';

const propTypes = {
  cycles: PropTypes.number.isRequired,
  restTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  exerciseTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  prepareTime: PropTypes.arrayOf(PropTypes.number).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  minutes: PropTypes.arrayOf(PropTypes.string).isRequired,
  seconds: PropTypes.arrayOf(PropTypes.string).isRequired,
  unitData: PropTypes.arrayOf(PropTypes.string).isRequired,
};

const Container = styled(View)`
  width: 100%;
  height: 100%;
  padding-top: ${spacings.padding2};
  background-color: ${colors.black};
  padding-top: 5%;
  padding-bottom: 2%;
  justify-content: space-between;
  align-items: center;
`;

const StyledHeading = styled(Text)`
  font-size: ${sizes.text2};
  text-align: center;
  color: ${colors.white};
  font-family: ${fonts.rubik};
`;

const StyledText = styled(Text)`
  font-size: ${sizes.text4};
  text-align: left;
  color: ${colors.white};
  font-family: ${fonts.rubik};
`;

const StyledTime = styled(Text)`
  font-size: ${sizes.text4};
  text-align: left;
  color: ${colors.white};
  font-family: ${fonts.rubik};
`;

const ResultsContainer = styled(View)`
  width: 90%;
  padding-left: 5%;
  padding-right: 5%;
  padding-top: 5%;
  padding-bottom: 5%;
  background-color: transparent;
  border-width: 2;
  border-color: ${colors.yellow};
  border-radius: ${spacings.radius};
`;

const InfoContainer = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 1%;
  padding-bottom: 1%;
  padding-left: 5%;
  padding-right: 5%;
`;

const Done = styled(View)`
  width: 90%;
  background-color: ${colors.yellow};
  justify-content: center;
  align-items: center;
  border-radius: ${spacings.radius};
  height: 16%;
`;

const DoneText = styled(Text)`
  color: ${colors.black};
  font-size: ${sizes.text1};
  font-family: ${fonts.rubikBoldItalic};
  width: 100%;
  text-align: center;
`;

class ResultsScreen extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleNavigateToBasicTaba);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleNavigateToBasicTaba);
  }

  handleNavigateToBasicTaba = () => {
    this.props.navigation.navigate('BasicTabata');
    return true;
  }

  render() {
    const {
      cycles,
      restTime,
      exerciseTime,
      prepareTime,
      minutes,
      seconds,
      unitData,
    } = this.props;
    const cyclesValue = unitData[cycles];
    const restTimeInSeconds = getTimeInSeconds(restTime, minutes, seconds) * (cyclesValue - 1);
    const sumRestTime = secondsToTime(restTimeInSeconds, 'string');
    const exerciseTimeInSeconds = getTimeInSeconds(exerciseTime, minutes, seconds) * cyclesValue;
    const sumExerciseTime = secondsToTime(exerciseTimeInSeconds, 'string');
    const prepareTimeInSeconds = getTimeInSeconds(prepareTime, minutes, seconds);
    const sumPrepareTime = secondsToTime(prepareTimeInSeconds, 'string');
    const totalTime = secondsToTime(restTimeInSeconds + exerciseTimeInSeconds + prepareTimeInSeconds, 'string');

    return (
      <Container>
        <StyledHeading>Good Job!</StyledHeading>
        <ResultsContainer>
          <InfoContainer>
            <StyledText>Rounds</StyledText>
            <StyledTime>{cyclesValue}</StyledTime>
          </InfoContainer>

          <InfoContainer>
            <StyledText>Rest</StyledText>
            <StyledTime>{sumRestTime}</StyledTime>
          </InfoContainer>

          <InfoContainer>
            <StyledText>Exercise</StyledText>
            <StyledTime>{sumExerciseTime}</StyledTime>
          </InfoContainer>

          <InfoContainer>
            <StyledText>Prepare</StyledText>
            <StyledTime>{sumPrepareTime}</StyledTime>
          </InfoContainer>

          <InfoContainer>
            <StyledText>Total</StyledText>
            <StyledTime>{totalTime}</StyledTime>
          </InfoContainer>
        </ResultsContainer>
        <TouchableWithoutFeedback onPress={this.handleNavigateToBasicTaba}>
          <Done>
            <DoneText>DONE!</DoneText>
          </Done>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  cycles: state.cycles,
  restTime: state.restTime,
  exerciseTime: state.exerciseTime,
  prepareTime: state.prepareTime,
  minutes: state.minutes,
  seconds: state.seconds,
  unitData: state.unitData,
})

ResultsScreen.propTypes = propTypes;
const ConnectedResultsScreen = connect(mapStateToProps)(ResultsScreen)

export default ConnectedResultsScreen;
