import React from 'react';
import { Svg, Path } from 'react-native-svg';

const Start = props => (
  <Svg {...props}>
    <Path
      d="M250 0C112.07 0 0 111.79 0 249.39S112.07 500 250 500s250-113 250-250.61S387.93 0 250 0zm108.57 260.12l-143.5 78.08c-11 7-21.07 2-21.07-11V167c0-12 10-17 21.07-11l143.5 80.09c8.03 4.01 14.05 17.02 0 24.03z"
      />
  </Svg>
);

export default Start;
