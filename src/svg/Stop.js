import React from 'react';
import { Svg, Path } from 'react-native-svg';

const Stop = props => (
  <Svg {...props}>
    <Path
      d="M250 0C111.93 0 0 111.93 0 250s111.93 250 250 250 250-111.93 250-250S388.07 0 250 0zm-15.62 320.31a23.44 23.44 0 0 1-46.88 0V179.69a23.44 23.44 0 0 1 46.88 0zm78.12 0a23.44 23.44 0 0 1-46.88 0V179.69a23.44 23.44 0 0 1 46.88 0z"
      />
  </Svg>
);

export default Stop;
