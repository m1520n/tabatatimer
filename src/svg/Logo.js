import React from 'react';
import { Svg, Path, Text } from 'react-native-svg';
import { colors, fonts } from '../styles/styles';

const Logo = props => (
  <Svg {...props}>
    <Path
      fill={colors.black}
      stroke={colors.yellow}
      strokeWidth={30}
      d="M108.3 250.3c0 77.8 63.3 141 141 141s141-63.3 141-141-63.3-141-141-141"
    />
    <Path
      fill={colors.black}
      stroke={colors.white}
      strokeWidth={30}
      d="M239.3 109.4c-72.3 0-131.1 58.8-131.1 131.1"
    />
    <Text
      fill={colors.yellow}
      fontFamily={fonts.rubikMediumItalic}
      fontSize={185}
      transform="matrix(1 0 0 1 183.2266 325.8086)"
    >
      T
    </Text>
  </Svg>
)

export default Logo;
