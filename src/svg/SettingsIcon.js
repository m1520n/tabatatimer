import React from 'react';
import { Svg, Rect } from 'react-native-svg';

const SettingsIcon = props => (
  <Svg {...props}>
    <Rect x={162} width={288} height={60} rx={30} />
    <Rect y={122} width={450} height={60} rx={30} />
    <Rect x={90} y={244} width={360} height={60} rx={30} />
  </Svg>
)

export default SettingsIcon;
