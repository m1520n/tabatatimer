import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { WheelPicker } from 'react-native-wheel-picker-android';
import styled from 'styled-components';

import { colors, fonts, sizes, spacings } from '../styles/styles';

const propTypes = {
  cycles: PropTypes.number.isRequired,
  unitData: PropTypes.arrayOf(PropTypes.string).isRequired,
  bgColor: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  onUpdateCycles: PropTypes.func.isRequired,
  shadowColor: PropTypes.string.isRequired,
};

const Container = styled(View)`
  flex-direction: row;
  width: 100%;
  height: 16%;
  overflow: hidden;
  background-color: ${({ bgColor }) => bgColor};
  justify-content: space-between;
  align-items: center;
  border-radius: ${spacings.radius};
  margin-top: ${spacings.margin3};
  margin-bottom: ${spacings.margin3};
  padding-left: ${spacings.padding2};
  padding-right: ${spacings.padding1};
  padding-top: ${spacings.padding3};
  padding-bottom: ${spacings.padding3};
`;

const StyledWheelPicker = styled(WheelPicker)`
  width: 100%;
  height: ${spacings.wpHeight};
  border-radius: ${spacings.radius};
`;

const RowCycles = styled(View)`
  flex-direction: row;
  padding-left: ${spacings.padding2};
`;

const StyledText = styled(Text)`
  font-size: ${sizes.text4};
  font-family: ${fonts.rubik};
  color: ${colors.white};
`;

const Cycles = ({ cycles, onUpdateCycles, unitData, bgColor, shadowColor, heading }) => (
  <Container bgColor={bgColor} shadowColor={shadowColor}>
    <StyledText>{heading}</StyledText>
    <RowCycles>
      <StyledWheelPicker
        onItemSelected={onUpdateCycles}
        isCurved
        isCyclic
        itemTextColor={colors.white}
        selectedItemPosition={cycles}
        data={unitData}
        isAtmospheric
        itemTextSize={sizes.wheelpicker}
        itemTextFontFamily={fonts.rubik}
      />
    </RowCycles>
  </Container>
);

Cycles.propTypes = propTypes;

export default Cycles;
