import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors, fonts, sizes, spacings } from '../styles/styles';

import TimeSelector from './TimeSelector';

const propTypes = {
  time: PropTypes.arrayOf(PropTypes.number).isRequired,
  bgColor: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  justify: PropTypes.string,
  minutes: PropTypes.arrayOf(PropTypes.string).isRequired,
  seconds: PropTypes.arrayOf(PropTypes.string).isRequired,
  stateField: PropTypes.string.isRequired,
  onUpdateTime: PropTypes.func.isRequired,
  shadowColor: PropTypes.string.isRequired,
};

const defaultProps = {
  justify: 'space-between',
}

const Container = styled(View)`
  flex-direction: row;
  width: 100%;
  height: 16%;
  overflow: hidden;
  background-color: ${({ bgColor }) => bgColor};
  justify-content: ${({ justify }) => justify ? `${justify}` : 'space-between'};
  align-items: center;
  border-radius: ${spacings.radius};
  margin-top: ${spacings.margin3};
  margin-bottom: ${spacings.margin3};
  padding-left: ${spacings.padding2};
  padding-right: ${spacings.padding2};
`;

const StyledText = styled(Text)`
  font-size: ${sizes.text4};
  font-family: ${fonts.rubik};
  color: ${colors.white};
`;

const ExerciseStep = ({
  bgColor,
  shadowColor,
  justify,
  heading,
  time,
  minutes,
  seconds,
  onUpdateTime,
  stateField,
}) => (
  <Container justify={justify} bgColor={bgColor} shadowColor={shadowColor}>
    <StyledText>{heading}</StyledText>
    <TimeSelector
      time={time}
      minutes={minutes}
      seconds={seconds}
      onUpdateTime={onUpdateTime}
      stateField={stateField}
    />
  </Container>
);

ExerciseStep.propTypes = propTypes;
ExerciseStep.defaultProps = defaultProps;

export default ExerciseStep;
