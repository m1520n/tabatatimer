import React from 'react';
import { Text, View } from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { fonts, colors, sizes } from '../styles/styles';

const propTypes = {
  totalTime: PropTypes.string.isRequired,
};

const MainContainer = styled(View)`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-bottom: 7%;
`;

const StyledText = styled(Text)`
  font-size: ${sizes.heading1};
  color: ${colors.white};
  font-family: ${fonts.rubik};
`;

const Time = ({ totalTime }) => (
  <MainContainer>
    <StyledText>Total {totalTime}</StyledText>
  </MainContainer>
);

Time.propTypes = propTypes;

export default Time;
