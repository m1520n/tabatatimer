import React from 'react';
import { View } from 'react-native';
import { WheelPicker } from 'react-native-wheel-picker-android';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { fonts, sizes, spacings, colors } from '../styles/styles';

const propTypes = {
  time: PropTypes.arrayOf(PropTypes.number).isRequired,
  stateField: PropTypes.string.isRequired,
  minutes: PropTypes.arrayOf(PropTypes.string).isRequired,
  seconds: PropTypes.arrayOf(PropTypes.string).isRequired,
  onUpdateTime: PropTypes.func.isRequired,
};

const Container = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const StyledWheelPicker = styled(WheelPicker)`
  width: ${spacings.wpWidth};
  height: ${spacings.wpHeight / 2};
  border-radius: ${spacings.radius};
`;

const TimeSelector = ({
  onUpdateTime,
  time,
  stateField,
  minutes,
  seconds,
}) => (
  <Container>
    <StyledWheelPicker
      // eslint-disable-next-line react/jsx-no-bind
      onItemSelected={e => onUpdateTime(e, 0, stateField)}
      isCurved
      isCyclic
      itemTextColor={colors.white}
      selectedItemPosition={parseInt(time[0], 10)}
      data={minutes}
      isAtmospheric
      visibleItemCount={3}
      itemTextSize={sizes.wheelpicker}
      itemTextFontFamily={fonts.rubik}
    />

    <StyledWheelPicker
      // eslint-disable-next-line react/jsx-no-bind
      onItemSelected={e => onUpdateTime(e, 1, stateField)}
      isCurved
      isCyclic
      itemTextColor={colors.white}
      selectedItemPosition={parseInt(time[1], 10)}
      data={seconds}
      isAtmospheric
      visibleItemCount={3}
      itemTextSize={sizes.wheelpicker}
      itemTextFontFamily={fonts.rubik}
    />
  </Container>
);

TimeSelector.propTypes = propTypes;

export default TimeSelector;
