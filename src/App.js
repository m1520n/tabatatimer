import { createStackNavigator, createAppContainer } from 'react-navigation';

import BasicTabata from './screens/BasicTabata';
import SplashScreen from './screens/SplashScreen';
import StateMachine from './screens/StateMachine';
import Settings from './screens/Settings';
import ResultsScreen from './screens/ResultsScreen';

import FirebaseService from './services/firebase.service';
import SoundService from './services/sound.service';

const { playSound } = new SoundService();
const { getAdRequest } = new FirebaseService();

const MainNavigator = createStackNavigator({
  SplashScreen: { screen: SplashScreen, },
  BasicTabata: { screen: BasicTabata },
  StateMachine: {
    screen: StateMachine,
    params: {
      adRequest: getAdRequest(),
      playSound,
    },
  },
  Settings: { screen: Settings },
  ResultsScreen: { screen: ResultsScreen },
},
{
  headerMode: 'none',
})

const App = createAppContainer(MainNavigator);

// eslint-disable-next-line
console.reportErrorsAsExceptions = false;

export default App;
