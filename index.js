import React from 'react';
import { createStore, applyMiddleware } from 'redux';

import { AppRegistry, StatusBar } from 'react-native';
import { Provider } from 'react-redux';

import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import createSagaMiddleware from 'redux-saga';

import App from './src/App';
import reducer from './src/store/reducers/reducer';
import rootSaga from './src/sagas/saga';

import { colors } from './src/styles/styles';

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: 'root',
  storage,
};

const pReducer = persistReducer(persistConfig, reducer);

export const store = createStore(
  pReducer,
  applyMiddleware(sagaMiddleware),
);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

const RNRedux = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <StatusBar backgroundColor={colors.black} />
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent('trainingo', () => RNRedux);
